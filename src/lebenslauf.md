---
title: 'Lebenslauf'
permalink: '/lebenslauf/index.html'
cvLanguage: 'de'
layout: 'layouts/cv.njk'
summary: 'Hi, ich bin Thomas, Software- & Web-Entwickler aus Leidenschaft. In meiner Karriere habe ich bereits an vielen unterschiedlichen Projekten mit diversen Technologien gearbeitet. Eine meiner größten Stärken ist es, mich Problemen anzunehmen und Strategien zur deren Lösung zu entwickeln.'
---
