---
title: 'Today I Learned'
layout: 'layouts/til.njk'
---

Those are short snippets of findings which are too short to make it into a blog post.
I have a short list of those already in my Notion space, but like to share some of them with you which I think are most interesting.