---
title: Firefox reader mode
date: '2019-12-13'
---

You need at least one `<p>` tag around the text you want to see in Reader View and at least 516 characters in 7 words inside the text.
