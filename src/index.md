---
layout: home
title: Hi, I’m Thomas
postsHeading: Latest posts
archiveButtonText: See all posts
metaTitle: Thomas Massmann
metaDesc: 'I’m software and web developer at it-spirit. I focus on accessibility and inclusive design.'
socialImage: ''
---

I’m software and web developer at [it-spirit](https://www.it-spir.it). I focus on accessibility and inclusive design.
