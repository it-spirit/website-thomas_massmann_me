---
title: collective.prettyphoto for Plone
date: '2009-12-31'
summary: 'Over the past few days I had some time to finish what was planned for so long: integrate prettyPhoto (written by Stéphane Caron) into Plone. Here it is...'
tags:
  - Plone
aliases:
  - /blog/2009-12-31-collectiveprettyphoto-for-plone.html
  - /blog/2009/12/31/collectiveprettyphoto-for-plone
  - /en/blog/collective-prettyphoto-for-plone
  - /articles/collective-prettyphoto-for-plone
---

The original implementation by Stéphane Caron can be found [here](http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/).

This plugin has been tested and is known to work in the following browsers:

- Firefox 2.0+
- Safari 3.1.1+
- Opera 9+
- Internet Explorer 6.0+

`collective.prettyphoto` is an implementation of prettyPhoto for Plone.

## Installation

`collective.prettyphoto` is availabe at [PyPI](http://pypi.python.org/pypi/collective.prettyphoto). It requires Plone 3.x or later (tested on 3.3.3).

### Installation without buildout

Install this package in either your system path packages or in the `lib/python` directory of your Zope instance. You can do this using either `easy_install` or via the `setup.py` script.

### Installation with buildout

If you are using [buildout](http://pypi.python.org/pypi/zc.buildout) to manage your instance installing `collective.prettyphoto` is even simpler. You can install `collective.prettyphoto` by adding it to the eggs line for your instance:

```ini
[instance]
eggs = collective.prettyphoto
```

After updating the configuration you need to run the `bin/buildout`, which will take care of updating your system.

## Usage

`collective.prettyphoto` adds a new view for Topics, Folders and Large Plone Folders: **Thumbnail view (prettyPhoto)**.

To use prettyPhoto for inline elements add `prettyPhoto` from the styles menu (Kupu and TinyMCE) to the link.

## Configuration

`collective.prettyphoto` can be customized via property sheet (go to ZMI, portal_properties, prettyphoto_properties).

- `theme`:
    - `dark_rounded`
    - `dark_square`
    - `facebook`
    - `light_rounded` (default)
    - `light_square`
- `speed`:
    - `fast`
    - `normal` (default)
    - `slow`
- `opacity`: value from 0.0 to 1.0 (default: 0.80)
- `show_title`: show the title for images? (default: True)
- `counter_sep`: the separator for the gallery counter 1 "of" 2 (default: "/")
- `autoplay`: automatically start videos? (default: True)
- `iframe_width`: the width of the iframe (must be percantage, default: 75%)
- `iframe_height`: the height of the iframe (must be percentage, default: 75%)