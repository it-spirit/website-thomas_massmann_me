---
title: 'Curriculum Vitae'
permalink: '/cv/index.html'
cvLanguage: 'en'
layout: 'layouts/cv.njk'
summary: 'Hi, I’m Thomas. I’m a software and web developer with a strong focus on accessibility and inclusive design. During my career in software development I learned and used a lot of different tools and techniques. One of my biggest strength is to analyze problems and find strategies to solve them.'
---
