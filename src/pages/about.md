---
title: 'About Me'
permalink: '/about/index.html'
summary: 'Hi, I’m Thomas. I’m a software and web developer with a strong focus on accessibility and inclusive design.'
---

![Thomas Massmann](https://res.cloudinary.com/it-spirit/image/upload/c_thumb,f_auto,g_face,w_350/v1589205747/thomas.massmann.me/thomasmassmann.jpg 'I like wearing sunglasses')

I like to solve problems (at least that’s what I tell myself from time to time to not get frustrated). I live with the love of my life and our wonderful daughter in Kaufbeuren, Germany. When I’m not coding or doing consulting stuff, I love to work in our garden where we grow our own vegetables. I used to play tennis for more than 20 years, but well, times change.

For those who are interersted in my professional career I added my [Curriculum Vitae](/cv) on a separate page (also [available in german](/lebenslauf)).

## Home on the web

You can find me in the following places on the world wide web:

- [Gitlab](https://gitlab.com/thomasmassmann) - gitlab.com/thomasmassmann
- [GitHub](https://github.com/thomasmassmann) - github.com/thomasmassmann
- [Twitter](https://twitter.com/thomasmassmann) - twitter.com/thomasmassmann
- [LinkedIn](https://www.linkedin.com/in/thomasmassmann) - linkedin.com/in/thomasmassmann
- [Xing](https://www.xing.com/profile/Thomas_Massmann4) - xing.com/profile/Thomas_Massmann4

## About this site

This site is built with ❤️ using [Eleventy](https://www.11ty.dev) and powered by the excellent [Hylia Eleventy Starter Kit](https://hylia.website) made by [Andy Bell](https://hankchizljaw.com/).

The code for this website is [available at Gitlab](https://gitlab.com/it-spirit/website-thomas_massmann_me). Hosting is done by the wonderful people at [Netlify](https://www.netlify.com). It is delivered over HTTPS thanks to a free certificate from Let’s Encrypt.
