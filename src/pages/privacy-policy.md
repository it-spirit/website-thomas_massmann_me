---
title: Privacy Policy
permalink: '/privacy-policy/index.html'
---

Personal data (usually referred to just as "data" below) will only be processed by us to the extent necessary and for the purpose of providing a functional and user-friendly website, including its contents, and the services offered there.

Per Art. 4 No. 1 of Regulation (EU) 2016/679, i.e. the General Data Protection Regulation (hereinafter referred to as the "GDPR"), "processing" refers to any operation or set of operations such as collection, recording, organization, structuring, storage, adaptation, alteration, retrieval, consultation, use, disclosure by transmission, dissemination, or otherwise making available, alignment, or combination, restriction, erasure, or destruction performed on personal data, whether by automated means or not.

The following privacy policy is intended to inform you in particular about the type, scope, purpose, duration, and legal basis for the processing of such data either under our own control or in conjunction with others. We also inform you below about the third-party components we use to optimize our website and improve the user experience which may result in said third parties also processing data they collect and control.

Our privacy policy is structured as follows:

I. Information about us as controllers of your data<br>
II. The rights of users and data subjects<br>
III. Information about the data processing<br>

## I. Information about us as controllers of your data

The party responsible for this website (the "controller") for purposes of data protection law is:

Thomas Massmann<br>
Regenleitenstr. 13<br>
87600 Kaufbeuren<br>
Germany

Telephone: +49-08341-9966186<br>
Fax: Fax +49-08341-9966186-9<br>
Email: thomas@massmann.me

## II. The rights of users and data subjects

With regard to the data processing to be described in more detail below, users and data subjects have the right

- to confirmation of whether data concerning them is being processed, information about the data being processed, further information about the nature of the data processing, and copies of the data (cf. also Art. 15 GDPR);
- to correct or complete incorrect or incomplete data (cf. also Art. 16 GDPR);
- to the immediate deletion of data concerning them (cf. also Art. 17 DSGVO), or, alternatively, if further processing is necessary as stipulated in Art. 17 Para. 3 GDPR, to restrict said processing per Art. 18 GDPR;
- to receive copies of the data concerning them and/or provided by them and to have the same transmitted to other providers/controllers (cf. also Art. 20 GDPR);
- to file complaints with the supervisory authority if they believe that data concerning them is being processed by the controller in breach of data protection provisions (see also Art. 77 GDPR).

In addition, the controller is obliged to inform all recipients to whom it discloses data of any such corrections, deletions, or restrictions placed on processing the same per Art. 16, 17 Para. 1, 18 GDPR. However, this obligation does not apply if such notification is impossible or involves a disproportionate effort. Nevertheless, users have a right to information about these recipients.

Likewise, under Art. 21 GDPR, users and data subjects have the right to object to the controller’s future processing of their data pursuant to Art. 6 Para. 1 lit. f) GDPR. In particular, an objection to data processing for the purpose of direct advertising is permissible.

## III. Information about the data processing

Your data processed when using our website will be deleted or blocked as soon as the purpose for its storage ceases to apply, provided the deletion of the same is not in breach of any statutory storage obligations or unless otherwise stipulated below.

### Server data

For technical reasons, the following data sent by your internet browser to us or to our server provider will be collected, especially to ensure a secure and stable website: These server log files record the type and version of your browser, operating system, the website from which you came (referrer URL), the webpages on our site visited, the date and time of your visit, as well as the IP address from which you visited our site.

The data thus collected will be temporarily stored, but not in association with any other of your data.

The basis for this storage is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in the improvement, stability, functionality, and security of our website.

The data will be deleted within no more than seven days, unless continued storage is required for evidentiary purposes. In which case, all or part of the data will be excluded from deletion until the investigation of the relevant incident is finally resolved.

### Contact

If you contact us via email or the contact form, the data you provide will be used for the purpose of processing your request. We must have this data in order to process and answer your inquiry; otherwise we will not be able to answer it in full or at all.

The legal basis for this data processing is Art. 6 Para. 1 lit. b) GDPR.

Your data will be deleted once we have fully answered your inquiry and there is no further legal obligation to store your data, such as if an order or contract resulted therefrom.

### User posts, comments, and ratings

We offer you the opportunity to post questions, answers, opinions, and ratings on our website, hereinafter referred to jointly as "posts." If you make use of this opportunity, we will process and publish your post, the date and time you submitted it, and any pseudonym you may have used.

The legal basis for this is Art. 6 Para. 1 lit. a) GDPR. You may revoke your prior consent under Art. 7 Para. 3 GDPR with future effect. All you have to do is inform us that you are revoking your consent.

In addition, we will also process your IP address and email address. The IP address is processed because we might have a legitimate interest in taking or supporting further action if your post infringes the rights of third parties and/or is otherwise unlawful.

In this case, the legal basis is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in any legal defense we may have to mount.

### Follow-up comments

If you make posts on our website, we also offer you the opportunity to subscribe to any subsequent follow-up comments made by third parties. In order to be able to inform you about these follow-up comments, we will need to process your email address.

The legal basis for this is Art. 6 Para. 1 lit. a) GDPR. You may revoke your prior consent to this subscription under Art. 7 Para. 3 GDPR with future effect. All you have to do is inform us that you are revoking your consent or click on the unsubscribe link contained in each email.

### Twitter

We maintain an online presence on Twitter to present our company and our services and to communicate with customers/prospects. Twitter is a service provided by Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA.

We would like to point out that this might cause user data to be processed outside the European Union, particularly in the United States. This may increase risks for users that, for example, may make subsequent access to the user data more difficult. We also do not have access to this user data. Access is only available to Twitter. Twitter Inc. is certified under the Privacy Shield and committed to adhering to European privacy standards.

[https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active)

The privacy policy of Twitter can be found at

[https://twitter.com/privacy](https://twitter.com/privacy)

### Social media links via graphics

We also integrate the following social media sites into our website. The integration takes place via a linked graphic of the respective site. The use of these graphics stored on our own servers prevents the automatic connection to the servers of these networks for their display. Only by clicking on the corresponding graphic will you be forwarded to the service of the respective social network.

Once you click, that network may record information about you and your visit to our site. It cannot be ruled out that such data will be processed in the United States.

Initially, this data includes such things as your IP address, the date and time of your visit, and the page visited. If you are logged into your user account on that network, however, the network operator might assign the information collected about your visit to our site to your personal account. If you interact by clicking Like, Share, etc., this information can be stored your personal user account and possibly posted on the respective network. To prevent this, you need to log out of your social media account before clicking on the graphic. The various social media networks also offer settings that you can configure accordingly.

The following social networks are integrated into our site by linked graphics:

### twitter

Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA

Privacy Policy: [https://twitter.com/privacy](https://twitter.com/privacy)

EU-US Privacy Shield

[https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Activeh](https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active)

### YouTube

We use YouTube on our website. This is a video portal operated by YouTube LLC, 901 Cherry Ave, 94066 San Bruno, CA, USA, hereinafter referred to as "YouTube".

YouTube is a subsidiary of Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland, hereinafter referred to as "Google".

Through certification according to the EU-US Privacy Shield

[https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&status=Active](https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&status=Active)

Google and its subsidiary YouTube guarantee that they will follow the EU’s data protection regulations when processing data in the United States.

We use YouTube in its advanced privacy mode to show you videos. The legal basis is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in improving the quality of our website. According to YouTube, the advanced privacy mode means that the data specified below will only be transmitted to the YouTube server if you actually start a video.

Without this mode, a connection to the YouTube server in the USA will be established as soon as you access any of our webpages on which a YouTube video is embedded.

This connection is required in order to be able to display the respective video on our website within your browser. YouTube will record and process at a minimum your IP address, the date and time the video was displayed, as well as the website you visited. In addition, a connection to the DoubleClick advertising network of Google is established.

If you are logged in to YouTube when you access our site, YouTube will assign the connection information to your YouTube account. To prevent this, you must either log out of YouTube before visiting our site or make the appropriate settings in your YouTube account.

For the purpose of functionality and analysis of usage behavior, YouTube permanently stores cookies on your device via your browser. If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Further details can be found in the section about cookies above.

Further information about the collection and use of data as well as your rights and protection options in Google’s privacy policy found at

[https://policies.google.com/privacy](https://policies.google.com/privacy)

### Vimeo

We use Vimeo to display videos on our website. This is a service of Vimeo, LLC, 555 West 18th Street, New York, New York 10011, USA, hereinafter referred to as "Vimeo."

Some of the user data is processed on Vimeo servers in the USA. Through certification according to the EU-US Privacy Shield

[https://www.privacyshield.gov/participant?id=a2zt00000008V77AAE&status=Active](https://www.privacyshield.gov/participant?id=a2zt00000008V77AAE&status=Active)

Vimeo guarantees that it will follow the EU’s data protection regulations when processing data in the United States.

The legal basis for collecting and processing this information is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in improving the quality of our website.

When you access a page on our website that has Vimeo embedded in it, a connection to Vimeo’s servers in the USA is established. For technical reasons, it is necessary for Vimeo to process your IP address. In addition, the date and time of your visit to our website will also be recorded.

If you are logged into Vimeo while visiting one of our webpages containing a Vimeo video, Vimeo will associate information about your interaction with our site to your Vimeo account. If you wish to prevent this, you must either log out of Vimeo before visiting our website or configure your Vimeo user account accordingly.

For the purpose of functionality and usage analysis, Vimeo uses Google Analytics. Google Analytics stores cookies on your terminal device via your browser and transmits information about the use of those websites in which a Vimeo video is embedded to Google. It is possible that Google will process this information in the USA.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Further details can be found in the section about cookies above.

The legal basis for collecting and processing this information is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in improving the quality of our website and in Vimeo’s legitimate interest in statistically analyzing user behavior for optimization and marketing purposes.

Vimeo offers further information about its data collection and processing as well your rights and your options for protecting your privacy at this link:

[http://vimeo.com/privacy](http://vimeo.com/privacy).

[Model Data Protection Statement](https://www.ratgeberrecht.eu/leistungen/muster-datenschutzerklaerung.html) for [Anwaltskanzlei Weiß & Partner](https://www.ratgeberrecht.eu/)
