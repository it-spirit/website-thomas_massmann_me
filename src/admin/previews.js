// import getCollectionItem from '@11ty/eleventy/Filters/GetCollectionItem';
const {w3DateFilter, markdownFilter, dateFilter, helpers, getCollectionItem} = previewUtil;

const env = nunjucks.configure();

env.addFilter('w3DateFilter', w3DateFilter);
env.addFilter('markdownFilter', markdownFilter);
env.addFilter('dateFilter', dateFilter);
env.addFilter('getPreviousCollectionItem', (collection, page) => {
  if (collection != undefined)
    getCollectionItem(collection, page, -1)
});
env.addFilter('getNextCollectionItem', (collection, page) => {
  if (collection != undefined)
    getCollectionItem(collection, page, 1)
});

const Preview = ({entry, path, context}) => {
  const data = context(entry.get('data').toJS());
  const html = env.render(path, {...data, helpers});
  return <div dangerouslySetInnerHTML={{__html: html}} />;
};

const Home = ({entry}) => (
  <Preview
    entry={entry}
    path="layouts/home.njk"
    context={({title, body, postsHeading, archiveButtonText}) => ({
      title,
      content: markdownFilter(body),
      postsHeading,
      archiveButtonText,
      collections: {
        postFeed: [
          {
            url: 'javascript:void(0)',
            date: new Date(),
            data: {
              title: 'Sample Post',
            },
          },
        ],
      },
    })}
  />
);

const Post = ({entry}) => (
  <Preview
    entry={entry}
    path="layouts/post.njk"
    context={({title, date, body}) => ({
      title,
      date,
      content: markdownFilter(body || ''),
    })}
  />
);

const Page = ({entry}) => (
  <Preview
    entry={entry}
    path="layouts/page.njk"
    context={({title, body}) => ({
      title,
      content: markdownFilter(body || ''),
    })}
  />
);

const CV = ({entry}) => (
  <Preview
    entry={entry}
    path="layouts/cv.njk"
    context={({title, summary, body}) => ({
      title,
      summary,
      content: markdownFilter(body || ''),
    })}
  />
);

const SiteData = ({entry}) => (
  <Preview
    entry={entry}
    path="layouts/base.njk"
    context={({name, shortDesc, showThemeCredit}) => ({
      site: {
        name,
        shortDesc,
        showThemeCredit,
      },
    })}
  />
);

const Nav = ({entry}) => (
  <Preview
    entry={entry}
    path="layouts/base.njk"
    context={({items}) => ({
      navigation: {
        items,
      },
    })}
  />
);

const FooterNav = ({entry}) => (
  <Preview
    entry={entry}
    path="layouts/base.njk"
    context={({items}) => ({
      footerlinks: {
        items,
      },
    })}
  />
);

const CVData = ({entry}) => (
  <div>
    <Preview
      entry={entry}
      path="layouts/cv.njk"
      context={({experience, education, skills}) => ({
        cvLanguage: 'en',
        cv: {
          experience,
          education,
          skills
        },
      })}
    />
    <hr />
    <Preview
      entry={entry}
      path="layouts/cv.njk"
      context={({experience, education, skills}) => ({
        cvLanguage: 'de',
        cv: {
          experience,
          education,
          skills
        },
      })}
    />
  </div>
);

CMS.registerPreviewTemplate('home', Home);
CMS.registerPreviewTemplate('posts', Post);
CMS.registerPreviewTemplate('generic_pages', Page);
CMS.registerPreviewTemplate('site_data', SiteData);
CMS.registerPreviewTemplate('nav', Nav);
CMS.registerPreviewTemplate('footer_nav', FooterNav);
CMS.registerPreviewTemplate('cv_data', CVData);
CMS.registerPreviewTemplate('cv', CV);
CMS.registerPreviewTemplate('lebenslauf', CV);
