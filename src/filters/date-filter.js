// Stolen from https://stackoverflow.com/a/31615643
const appendSuffix = n => {
  var s = ['th', 'st', 'nd', 'rd'],
    v = n % 100;
  return n + (s[(v - 20) % 10] || s[v] || s[0]);
};

module.exports = function dateFilter(value, showDays, lang) {
  if (showDays === undefined) {
    showDays = true;
  }
  if (lang === undefined) {
    lang = 'en';
  }
  const dateObject = new Date(value);

  let months;
  if (lang == 'de') {
    months = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
  } else {
    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  }
  const dayWithSuffix = appendSuffix(dateObject.getDate());
  let formattedDate = `${months[dateObject.getUTCMonth()]} ${dateObject.getFullYear()}`
  if (showDays) {
    formattedDate = `${dayWithSuffix} ${formattedDate}`;
  }
  return formattedDate;
};
